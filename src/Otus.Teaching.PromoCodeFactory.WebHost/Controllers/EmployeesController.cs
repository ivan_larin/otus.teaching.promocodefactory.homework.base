﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _roleRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> roleRepository)
        {
            _employeeRepository = employeeRepository;
            _roleRepository = roleRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> Create(EmployeeCreateUpdateRequest request)
        {
            Employee employee = await _employeeRepository.CreateAsync();
            return await CopyEmployeeFields(employee, request);
        }

        private async Task<ActionResult<EmployeeResponse>> CopyEmployeeFields(Employee employee, EmployeeCreateUpdateRequest request)
        {
            employee.AppliedPromocodesCount = request.AppliedPromocodesCount;
            employee.Email = request.Email;
            employee.FirstName = request.FirstName;
            employee.LastName = request.LastName;
            employee.Roles = new List<Role>();
            foreach (var roleGuid in request.RoleGuids)
            {
                Role role = await _roleRepository.GetByIdAsync(roleGuid);
                if (role == null)
                    return BadRequest();
                employee.Roles.Add(role);
            }

            return await GetEmployeeByIdAsync(employee.Id);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> Update(Guid id, EmployeeCreateUpdateRequest request)
        {
            Employee employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();
            return await CopyEmployeeFields(employee, request);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> Delete(Guid id)
        {
            Employee employee = await _employeeRepository.DeleteAsync(id);
            if (employee == null)
                return NotFound();
            return new JsonResult(employee);
        }
    }
}