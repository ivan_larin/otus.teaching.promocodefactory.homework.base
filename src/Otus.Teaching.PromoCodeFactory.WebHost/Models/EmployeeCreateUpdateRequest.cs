﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeCreateUpdateRequest
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<Guid> RoleGuids { get; set; } = new List<Guid>();

        public int AppliedPromocodesCount { get; set; }
    }
}
